#S04 Activity:
#1. Create an abstract class called Animal that has the following abstract methods#
    #Abstract Methods: eat(food), make_sound()
#2. Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:
    #Properties: name, breed, age 
    #Methods: getters and setters, implementation of abstract methods, call()

from abc import ABC, abstractclassmethod
class Animal(ABC):

    @abstractclassmethod

    def eat(self, food):
        pass

    def make_sound(self):
        pass

    
    

class Dog(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    # Getter
    def get_name(self):
        print(f"Name of dog: {self._name}")
    
    def get_breed(self):
        print(f"Breed of dog: {self._breed}")
    
    def get_age(self):
        print(f"Age of dog: {self._age}")

    # Setter
    def set_name(self, name):
        self._name = name
    
    def set_breed(self, breed):
        self._breed = breed

    def set_age(self, age):
        self._age = age

    # implementing the abstract methods from the parent class
    def eat(self, food):
        print(f"Eaten {food}")
    
    def call(self):
        print(f"Here {self._name}!")
    
    def make_sound(self):
        print("Bark! Woof! Arf!")

    

        
class Cat(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    # Getter
    def get_name(self):
        print(f"Name of cat: {self._name}")
    
    def get_breed(self):
        print(f"Breed of cat: {self._breed}")
    
    def get_age(self):
        print(f"Age of cat: {self._age}")
    
    # Setter
    def set_name(self, name):
        self._name = name
    
    def set_breed(self, breed):
        self._breed = breed

    def set_age(self, age):
        self._age = age

    # implementing the abstract methods from the parent class
    def eat(self, food):
        print(f"Serve me {food}")

    def call(self):
        print(f"{self._name}, come on!")
    
    def make_sound(self):
        print("Miaow! Nyaw! Nyaaaaa!")


# Test Cases:
dog1 = Dog("Isis", "dachshund", 8)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian Cat", 2)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()
